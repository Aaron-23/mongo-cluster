FROM mongo:4.0.16
MAINTAINER  zhangz@goodrain.com

ENV ES_DEFAULT_EXEC_ARGS=bash
ENV MONGO_VERSION=4.0.16
ENV DBPATH=/data/db
RUN apt-get update && \
    apt-get install -y dnsutils vim iproute && \
    apt-get clean 

COPY docker-entrypoint.sh /

COPY entrypoint.sh /point.sh
COPY replset.sh  /
COPY mongod.conf /etc/mongod.conf
VOLUME /data/db /data/configdb
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 27017

CMD ["mongod","-f","/etc/mongod.conf"]